//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by EpicForcePhysicsSceneExporter.rc
//


#define IDS_LIBDESCRIPTION              1
#define IDS_CATEGORY                    2
#define IDS_CLASS_NAME                  3
#define IDS_PARAMS                      4
#define IDS_SPIN                        5


#define IDD_PANEL                       101

#define IDC_CLOSEBUTTON                 1000
#define IDC_DOSTUFF                     1000
#define IDC_COLOR                       1456
#define IDC_EDIT                        1490
#define IDC_SPIN                        1506
#define IDC_PC							1600
#define IDC_IOS							1601
#define IDC_OK							1602
#define IDC_ANDROID						1603
#define IDC_PSVITA						1604
#define IDC_CANCEL						1605
#define IDC_WIIU						1606

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        101
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
