///////////////////////////////////////////////////////////////////
// Copyright(c) 2011-, EpicForce Entertainment Limited
// 
// Author : Mac Lin
// Module : EpicForceEngine
// Date   : 19/Aug/2011
// 
///////////////////////////////////////////////////////////////////
#include "EMath.h"
#include "Matrix2.h"
using namespace EpicForce;

const Matrix2 Matrix2::ZERO(
    0.0f,0.0f,
    0.0f,0.0f);
const Matrix2 Matrix2::IDENTITY(
    1.0f,0.0f,
    0.0f,1.0f);