///////////////////////////////////////////////////////////////////
// Copyright(c) 2011-, EpicForce Entertainment Limited
// 
// Author : Mac Lin
// Module : EpicForceEngine
// Date   : 19/Aug/2011
// 
///////////////////////////////////////////////////////////////////
#include "IVector3.h"
using namespace EpicForce;

const IVector3 IVector3::ZERO(0.0f,0.0f,0.0f);
const IVector3 IVector3::UNIT_X(1.0f,0.0f,0.0f);
const IVector3 IVector3::UNIT_Y(0.0f,1.0f,0.0f);
const IVector3 IVector3::UNIT_Z(0.0f,0.0f,1.0f);
const IVector3 IVector3::ONE(1.0f,1.0f,1.0f);