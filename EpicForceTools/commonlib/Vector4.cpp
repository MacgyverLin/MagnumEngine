///////////////////////////////////////////////////////////////////
// Copyright(c) 2011-, EpicForce Entertainment Limited
// 
// Author : Mac Lin
// Module : EpicForceEngine
// Date   : 19/Aug/2011
// 
///////////////////////////////////////////////////////////////////
#include "Vector4.h"
using namespace EpicForce;

const Vector4 Vector4::ZERO(0.0f,0.0f,0.0f,0.0f);
const Vector4 Vector4::UNIT_X(1.0f,0.0f,0.0f,0.0f);
const Vector4 Vector4::UNIT_Y(0.0f,1.0f,0.0f,0.0f);
const Vector4 Vector4::UNIT_Z(0.0f,0.0f,1.0f,0.0f);
const Vector4 Vector4::UNIT_W(0.0f,0.0f,0.0f,1.0f);
const Vector4 Vector4::ONE(1.0f,1.0f,1.0f,1.0f);