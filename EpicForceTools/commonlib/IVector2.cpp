///////////////////////////////////////////////////////////////////
// Copyright(c) 2011-, EpicForce Entertainment Limited
// 
// Author : Mac Lin
// Module : EpicForceEngine
// Date   : 19/Aug/2011
// 
///////////////////////////////////////////////////////////////////
#include "IVector2.h"
using namespace EpicForce;

const IVector2 IVector2::ZERO(0.0f,0.0f);
const IVector2 IVector2::UNIT_X(1.0f,0.0f);
const IVector2 IVector2::UNIT_Y(0.0f,1.0f);
const IVector2 IVector2::ONE(1.0f,1.0f);