///////////////////////////////////////////////////////////////////
// Copyright(c) 2011-, EpicForce Entertainment Limited
// 
// Author : Mac Lin
// Module : EpicForceEngine
// Date   : 19/Aug/2011
// 
///////////////////////////////////////////////////////////////////
#include "IVector4.h"
using namespace EpicForce;

const IVector4 IVector4::ZERO(0,0,0,0);
const IVector4 IVector4::UNIT_X(1,0,0,0);
const IVector4 IVector4::UNIT_Y(0,1,0,0);
const IVector4 IVector4::UNIT_Z(0,0,1,0);
const IVector4 IVector4::UNIT_W(0,0,0,1);
const IVector4 IVector4::ONE(1,1,1,1);