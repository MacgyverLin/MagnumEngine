#ifndef _CustomDialog_h_
#define _CustomDialog_h_

#include "wxdef.h"

class CustomDialog : public wxDialog
{
public:
	CustomDialog(const wxString& title);
};

#endif