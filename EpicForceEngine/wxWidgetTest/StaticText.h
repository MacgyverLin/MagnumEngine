#ifndef _StaticText_h_
#define _StaticText_h_

#include "wxdef.h"

class StaticText : public wxFrame
{
public:
	StaticText(const wxString& title);
};

#endif