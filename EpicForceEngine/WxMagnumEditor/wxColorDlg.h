#ifndef _wxColorDlg_h_
#define _wxColorDlg_h_

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#include "wxdef.h"
using namespace Magnum;

class wxColorDlg : public wxDialog
{
public:
	wxColorDlg(const wxString& title);
};

#endif