<VisualEffect order="2">
	<Attributes>
		<Attribute type="vec3" 	format="float3" name="inputPosition" 		semantic="POSITION0" />
		<Attribute type="vec3" 	format="float3" name="inputNormal" 			semantic="NORMAL" />
		<Attribute type="vec2" 	format="float2" name="inputTexCoord0" 		semantic="TEXCOORD0" />
		<Attribute type="vec4"  format="ubyte4"	name="inputBlendIndices"	semantic="BLEND4INDEX" />
		<Attribute type="vec4" 	format="float4"	name="inputBlendWeights"	semantic="BLEND4WEIGHT" />
	</Attributes>
	<Uniforms>
		<Uniform precision="" format="vec4" name="ambientLightColor" arraysize="1" semantic="" initializer="0.3,0.3,0.3,1.0"/>
		<Uniform precision="" format="vec3" name="dirLightDir" arraysize="1" semantic="LIGHT0VECTOR" initializer=""/>
		<Uniform precision="" format="vec4" name="dirLightColor" arraysize="1" semantic="LIGHT0COLOR" initializer=""/>
		<Uniform precision="" format="vec4" name="specularColor" arraysize="1" semantic="" initializer="0.7,0.7,0.7,1.0"/>
		<Uniform precision="" format="int" name="p" arraysize="1" semantic="" initializer="30"/> 
		<Uniform precision="" format="mat4" name="viewMat" arraysize="1" semantic="VIEW" initializer=""/>
		<Uniform precision="" format="mat4" name="worldViewMat" arraysize="1" semantic="WORLDVIEW" initializer=""/>
		<Uniform precision="" format="mat4" name="viewProjMat" arraysize="1" semantic="VIEWPROJ" initializer=""/>
		<Uniform precision="" format="sampler2D" name="diffuseMapSampler" arraysize="1" semantic="DIFFUSEMAP" initializer=""/>
		<Uniform precision="" format="sampler2D" name="glossMapSampler" arraysize="1" semantic="SPECULARMAP" initializer=""/>
		<Uniform precision="" format="mat4" name="boneMatrices" arraysize="20" semantic="BONETRANSFORMS" initializer=""/>
	</Uniforms> 
	<Technique name="Default">
		<Pass name="P0">
			<RenderStates>
        <RenderState type="cullenable" value="true"/>
        <RenderState type="cullmode" value="cw"/>
				<RenderState type="depthtestenable" value="true"/>
			</RenderStates>
			<VertexShader>
				attribute vec3 inputPosition;
				attribute vec3 inputNormal;
				attribute vec2 inputTexCoord0;
				attribute vec4 inputBlendIndices;
				attribute vec4 inputBlendWeights;
				
				varying vec4 varyingColor0;
				varying vec4 varyingColor1;
				varying vec2 varyingTexcoord0;
				
				uniform vec4 ambientLightColor;
				uniform vec4 dirLightColor;
				uniform vec3 dirLightDir;
				uniform vec4 specularColor;
				uniform int p;
				uniform mat4 viewMat;
				uniform mat4 worldViewMat;
				uniform mat4 viewProjMat;
				uniform mat4 boneMatrices[20];				
				
				mat3 xlat_lib_constructMat3(mat4 m)
				{ 
					return mat3(vec3(m[0]), vec3(m[1]), vec3(m[2]));
				} 
				
				vec3 convertModelToView(in vec3 dir)
				{
					return normalize( dir * xlat_lib_constructMat3(worldViewMat) );
				} 				
				
				vec3 convertWorldToView(in vec3 dir)
				{
					return normalize( dir * xlat_lib_constructMat3(viewMat) );
				} 
				
				vec3 blinnDiffuse(in vec3 L, in vec3 N )
				{ 
					return vec3(max(0.000000, dot(N, L))); 
				}
				
				vec3 blinnSpecular(in vec3 H, in vec3 N, in int p) 
				{ 
					return vec3(pow(max(0.000000, dot(N, H)), float(p)));
				}
				
				void main() 
				{ 
					vec3 V = vec3(0.000000, 0.000000, 1.00000); 
					vec3 N = convertModelToView(inputNormal);
					vec3 L = convertWorldToView(dirLightDir);   
					vec3 H = normalize(L + V);
					vec4 position			= vec4(inputPosition, 1.00000);
					
					gl_Position				= (position * boneMatrices[int(inputBlendIndices.x)] * inputBlendWeights.x +
											   position * boneMatrices[int(inputBlendIndices.y)] * inputBlendWeights.y +
											   position * boneMatrices[int(inputBlendIndices.z)] * inputBlendWeights.z +
											   position * boneMatrices[int(inputBlendIndices.w)] * inputBlendWeights.w) * viewProjMat;
					varyingColor0	= vec4(ambientLightColor.rgb + dirLightColor.rgb * blinnDiffuse(L, N), 1.00000);    
					varyingColor1	= vec4(dirLightColor.rgb * specularColor.rgb * blinnSpecular(H, N, p), 1.00000);    
					
					varyingTexcoord0 = inputTexCoord0;
				}
			</VertexShader>
			<PixelShader>
				varying vec4 varyingColor0;
				varying vec4 varyingColor1;
				varying vec2 varyingTexcoord0;
				
				uniform sampler2D diffuseMapSampler;
				uniform sampler2D glossMapSampler; 
				
				void main()
				{ 
					vec3 diffuseColor	= vec3(texture2D( diffuseMapSampler, varyingTexcoord0) );   
					vec3 glosssiness	= vec3(texture2D( glossMapSampler, varyingTexcoord0)   );   
					
					gl_FragData[0]		= vec4(varyingColor0.rgb * diffuseColor.rgb + varyingColor1.rgb * glosssiness.rgb, 1.00000); 
				} 
			</PixelShader>
		</Pass>
	</Technique>
</VisualEffect>