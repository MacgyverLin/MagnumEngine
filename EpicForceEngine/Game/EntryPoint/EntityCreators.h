#include "MacDemoAudioListenerEntity.h"
static Entity::Creator<MacDemoAudioListenerEntity> entityMacDemoAudioListener("MacDemoAudioListenerEntity");

#include "MacDemoAudioSourceEntity.h"
static Entity::Creator<MacDemoAudioSourceEntity> entityMacDemoAudioSource("MacDemoAudioSourceEntity");

#include "MacDemoCameraEntity.h"
static Entity::Creator<MacDemoCameraEntity> entityMacDemoCamera("MacDemoCameraEntity");

#include "MacDemoBGMEntity.h"
static Entity::Creator<MacDemoBGMEntity> entityMacDemoBGM("MacDemoBGMEntity");

#include "EditorEntity.h"
static Entity::Creator<EditorEntity> entityEditor("EditorEntity");

#include "MacDemoLevelEntity.h"
static Entity::Creator<MacDemoLevelEntity> entityMacDemoLevel("MacDemoLevelEntity");

#include "MacDemoPhysicsCubeEntity.h"
static Entity::Creator<MacDemoPhysicsCubeEntity> entityMacDemoPhysicsCube("MacDemoPhysicsCubeEntity");

#include "MacDemoPhysicsDiffuseMapCubeEntity.h"
static Entity::Creator<MacDemoPhysicsDiffuseMapCubeEntity> entityMacDemoPhysicsDiffuseMapCube("MacDemoPhysicsDiffuseMapCubeEntity");

#include "MacDemoPhysicsDiffuseMapSpecularMapCubeEntity.h"
static Entity::Creator<MacDemoPhysicsDiffuseMapSpecularMapCubeEntity> entityMacDemoPhysicsDiffuseMapSpecularMapCube("MacDemoPhysicsDiffuseMapSpecularMapCubeEntity");

#include "MacDemoPlaneEntity.h"
static Entity::Creator<MacDemoPlaneEntity> entityMacDemoPlane("MacDemoPlaneEntity");

#include "MacDemoParticleEmitterEntity.h"
static Entity::Creator<MacDemoParticleEmitterEntity> entityMacDemoParticleEmitter("MacDemoParticleEmitterEntity");

#include "MacDemoPhysics3SkeletalAnimModelEntity.h"
static Entity::Creator<MacDemoPhysics3SkeletalAnimModelEntity> entityMacDemoPhysics3SkeletalAnimModel("MacDemoPhysics3SkeletalAnimModelEntity");

#include "ReflectEntity.h"
static Entity::Creator<ReflectEntity> entityReflect("ReflectEntity");

#include "MacDemoSkySphereEntity.h"
static Entity::Creator<MacDemoSkySphereEntity> entityMacDemoSkySphere("MacDemoSkySphereEntity");

#include "MacDemoBillBoardEntity.h"
static Entity::Creator<MacDemoBillBoardEntity> entityMacDemoBillBoard("MacDemoBillBoardEntity");

#include "MacDemoCubeEntity.h"
static Entity::Creator<MacDemoCubeEntity> entityMacDemoCube("MacDemoCubeEntity");

#include "MacDemoDiffuseMapCubeEntity.h"
static Entity::Creator<MacDemoDiffuseMapCubeEntity> entityMacDemoDiffuseMapCube("MacDemoDiffuseMapCubeEntity");

#include "MacDemoDiffuseMapSpecularMapCubeEntity.h"
static Entity::Creator<MacDemoDiffuseMapSpecularMapCubeEntity> entityMacDemoDiffuseMapSpecularMapCube("MacDemoDiffuseMapSpecularMapCubeEntity");

