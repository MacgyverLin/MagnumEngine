#ifndef _MagnumGame_h_
#define _MagnumGame_h_

#include "MacDemoAudioListenerEntity.h"
#include "MacDemoAudioSourceEntity.h"
#include "MacDemoCameraEntity.h"
#include "MacDemoBGMEntity.h"
#include "EditorEntity.h"
#include "MacDemoLevelEntity.h"
#include "MacDemoPhysicsCubeEntity.h"
#include "MacDemoPhysicsDiffuseMapCubeEntity.h"
#include "MacDemoPhysicsDiffuseMapSpecularMapCubeEntity.h"
#include "MacDemoPlaneEntity.h"
#include "MacDemoParticleEmitterEntity.h"
#include "MacDemoPhysics3SkeletalAnimModelEntity.h"
#include "ReflectEntity.h"
#include "MacDemoSkySphereEntity.h"
#include "MacDemoBillBoardEntity.h"
#include "MacDemoCubeEntity.h"
#include "MacDemoDiffuseMapCubeEntity.h"
#include "MacDemoDiffuseMapSpecularMapCubeEntity.h"

#endif