#include "MacDemoScene.h"
static Scene::Creator<MacDemoScene> sceneMacDemo("MacDemo", false);

#include "ModelViewEditorScene.h"
static Scene::Creator<ModelViewEditorScene> sceneModelViewEditor("ModelViewEditor", false);

#include "PlayModeScene.h"
static Scene::Creator<PlayModeScene> scenePlayMode("PlayMode", false);

