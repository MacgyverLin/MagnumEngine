///////////////////////////////////////////////////////////////////////////////////
// Copyright(c) 2016, Lin Koon Wing Macgyver, macgyvercct@yahoo.com.hk			 //
//																				 //
// Author : Mac Lin									                             //
// Module : Magnum Engine v1.0.0												 //
// Date   : 14/Jun/2016											                 //
//																				 //
///////////////////////////////////////////////////////////////////////////////////
#include "EMath.h"
#include "Matrix2.h"
using namespace Magnum;

const Matrix2 Matrix2::ZERO(
    0.0f,0.0f,
    0.0f,0.0f);
const Matrix2 Matrix2::IDENTITY(
    1.0f,0.0f,
    0.0f,1.0f);