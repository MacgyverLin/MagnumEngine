///////////////////////////////////////////////////////////////////////////////////
// Copyright(c) 2016, Lin Koon Wing Macgyver, macgyvercct@yahoo.com.hk			 //
//																				 //
// Author : Mac Lin									                             //
// Module : Magnum Engine v1.0.0												 //
// Date   : 14/Jun/2016											                 //
//																				 //
///////////////////////////////////////////////////////////////////////////////////
//----------------------------------------------------------------------------
inline 
Capsule3::Capsule3 ()
{
    // uninitialized
}
//----------------------------------------------------------------------------
inline 
Capsule3::Capsule3 (const Segment3& rkSegment, float fRadius)
    :
    Segment(rkSegment)
{
    Radius = fRadius;
}
//----------------------------------------------------------------------------

