///////////////////////////////////////////////////////////////////////////////////
// Copyright(c) 2016, Lin Koon Wing Macgyver, macgyvercct@yahoo.com.hk			 //
//																				 //
// Author : Mac Lin									                             //
// Module : Magnum Engine v1.0.0												 //
// Date   : 14/Jun/2016											                 //
//																				 //
///////////////////////////////////////////////////////////////////////////////////
#include "IVector2.h"
using namespace Magnum;

const IVector2 IVector2::ZERO(0.0f,0.0f);
const IVector2 IVector2::UNIT_X(1.0f,0.0f);
const IVector2 IVector2::UNIT_Y(0.0f,1.0f);
const IVector2 IVector2::ONE(1.0f,1.0f);