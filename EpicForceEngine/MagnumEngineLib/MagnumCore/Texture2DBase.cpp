///////////////////////////////////////////////////////////////////////////////////
// Copyright(c) 2016, Lin Koon Wing Macgyver, macgyvercct@yahoo.com.hk			 //
//																				 //
// Author : Mac Lin									                             //
// Module : Magnum Engine v1.0.0												 //
// Date   : 14/Jun/2016											                 //
//																				 //
///////////////////////////////////////////////////////////////////////////////////
#include "Texture2DBase.h"
using namespace Magnum;

Texture2DBase::Texture2DBase()
: TextureBase()
{
}

Texture2DBase::~Texture2DBase()
{
}